import {
    APIGatewayProxyEvent,
    APIGatewayProxyResult
} from 'aws-lambda'
import { STATUS_CODE, SERVER_MESSAGE } from './config/constants'

export const sampleGet = async (
    event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> => {
    const queries = event.queryStringParameters
    if (!queries || !queries.body) {
        return {
            statusCode: STATUS_CODE.BAD_REQUEST,
            body: JSON.stringify({
                statusMsg: SERVER_MESSAGE.BAD_REQUEST
            })
        }
    }
    return {
            statusCode: STATUS_CODE.OK,
            body: JSON.stringify({
                statusMsg: SERVER_MESSAGE.OK
            })
        }
}

export const samplePost = async (
    event: APIGatewayProxyEvent
): Promise<APIGatewayProxyResult> => {
    const reqBody = JSON.parse(event.body)
    if (!reqBody || !reqBody.body) {
        return {
            statusCode: STATUS_CODE.BAD_REQUEST,
            body: JSON.stringify({
                statusMsg: SERVER_MESSAGE.BAD_REQUEST
            })
        }
    }
    return {
            statusCode: STATUS_CODE.OK,
            body: JSON.stringify({
                statusMsg: SERVER_MESSAGE.OK
            })
        }
}
