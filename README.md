# README #

Serverless Nodejs functinon prototype for development use

## Getting Started

1. install serverless
>npm install -g serverless

2. install aws-cli
-   please read https://cloudacademy.com/blog/how-to-use-aws-cli/

3. set aws configuration
> aws configure

4. local develop
-
> sls offline

5. deploy to lambda
> npm run deploy

## API GATEWAY

PATH: /getexample
method: get
example url: /getexample?body=a

PATH: /postexample
method: post
example body: {"body":"success"}